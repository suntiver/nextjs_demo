
import  {INCREMENT_COUNTER,DECREMENT_COUNTER, AUTHENTICATE, DEAUTHENTICATE} from '../type'


//key  rxre
const initialState = {
        token:null,
        user:null


}

export default (state = initialState, action) => {
    switch (action.type) {

    case AUTHENTICATE:
        return  {...state,token:action.payload}
    
    default:
        return state
    }
}
