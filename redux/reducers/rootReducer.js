import  { combineReducers }  from 'redux'
import  counterReducter from './counterReducter'
import authReducer  from './authReducer'

const  rootReducer =  combineReducers({

    counter:counterReducter,
    authReducer:authReducer


})


export  default rootReducer