import React from 'react'
import Head  from 'next/head'
import {connect} from 'react-redux'
import {incrementCounter,decrementCounter}  from  '../redux/actions/counterActions'

const  Home = (props) => {

  

  return (
    <div>
        

         <Head>
                   <title>Create Next App</title>
         </Head>

         <h1>Counter : {props.counter} </h1>
         <button  onClick={props.incrementCounter} >+</button>
         <button onClick={props.decrementCounter}>-</button>


    </div>
  )
}


// mapStateToProps
// รับฟังก์ชันจาก store มาใข้งาน

const  mapStateToProps  = state =>({

    counter : state.counter.value

})



//apDispatchToProps
// ส่งค่าไปยัง store เป็น Object

const   mapDispatchToProps = {
    incrementCounter : incrementCounter,
    decrementCounter : decrementCounter,


}


export default  connect(mapStateToProps,mapDispatchToProps)(Home)


