import "../styles/globals.css";
import { wrapper } from "../redux/store";
import Nav from '../components/Navbar'

function MyApp({ Component, pageProps }) {
  return (
    <div>
          <Nav/>
           <Component {...pageProps} />
    </div>
  );
}

export default wrapper.withRedux(MyApp);
